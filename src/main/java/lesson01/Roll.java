package lesson01;

public class Roll {
    private String player;
    private int result;

    public Roll(String player, int result) {
        this.player = player;
        this.result = result;
    }

    @Override
    public String toString() {
        return player + " - " + result;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
