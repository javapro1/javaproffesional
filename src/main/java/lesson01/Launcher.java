package lesson01;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Launcher {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (true) {
        System.out.println("Введите количество игроков: ");
        int numberOfPlayers = input.nextInt();
        String[] players = new String[numberOfPlayers];
        for (int i = 0; i < numberOfPlayers; i++) {
            System.out.println("Введите имя игрока " + (i + 1));
            players[i] = input.next();
            System.out.printf("Игрок %s: %s%n", i + 1, players[i]);
        }
        Game game = new Game(players);

            game.rollTheDice();
            printGameResults("Results", Arrays.asList(game.getRolls()));
            printGameResults("Winners", game.getWinners());

            System.out.println("Желаете ли повторить?: Y/N");
            String repeat = input.next();
            if (!repeat.equals("y")) {
                break;
            }
        }
    }

    private static void printGameResults(String header, List<Roll> rolls) {
        System.out.println("--- " + header + " ---");
        for (Roll roll : rolls) {
            System.out.println(roll.toString());
        }
    }
}
