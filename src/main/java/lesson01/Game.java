package lesson01;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private String[] players;
    private Roll[] rolls;

    public Game(String[] players) {
        this.players = players;
    }

    public void rollTheDice() {
        rolls = new Roll[players.length];
        for (int i = 0; i < players.length; i++) {
            rolls[i] = new Roll(players[i], choice());
        }
    }

    public List<Roll> getWinners() {
        List<Roll> winners = new ArrayList<>();
        int max = 0;
        for (Roll roll : rolls) {
            if (roll.getResult() > max) {
                max = roll.getResult();
                winners.clear();
                winners.add(roll);
            } else if (roll.getResult() == max) {
                winners.add(roll);
            }
        }
        return winners;
    }

    private static int choice() {
        int newNumber = (int) (Math.random() * 6 + 1);
        return newNumber;
    }

    public String[] getPlayers() {
        return players;
    }

    public Roll[] getRolls() {
        return rolls;
    }
}
